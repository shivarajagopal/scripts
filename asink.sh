#!/bin/bash
pactl list sinks | grep -e "Sink #" -e "Description" | sed ':a;N;$!ba;s/\n/ /g' | sed 's/Sink/\r\nSink/g'
echo Select a sink...
read sink_idx
sink_name=$(pactl list sinks | grep -e "Name" -e "Sink #" | sed ':a;N;$!ba;s/\n/ /g' | sed 's/Sink/\r\nSink/g' | grep "Sink #$sink_idx" | sed 's/^.*Name: //' | sed -e 's/^[ \t]*//')
echo $sink_name > /home/shiva/.asink
