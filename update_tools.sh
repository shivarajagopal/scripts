#!/bin/zsh
curl -L git.io/antigen > ~/scripts/antigen.zsh
cd ~/.skim
git pull
./install --all
cd
