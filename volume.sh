#!/bin/bash
direction="$1"
export SINK="$(cat /home/srajagopal/.asink | xargs)"
echo $SINK > /home/srajagopal/.debug
if [ $direction = "up" ]; then
  pactl set-sink-volume ${SINK} +5%
elif [ $direction = "down" ]; then
  pactl set-sink-volume ${SINK} -5%
elif [ $direction = "mute" ]; then
  pactl set-sink-mute ${SINK} toggle
fi
