#!/bin/bash

# make a tags directory. Vim will search up the directory structure from cwd until it finds a directory named "tags". 
# This is set up in ~/.vim/plugin/cscope_maps.vim for cscope.

# Make a tags directory to hold the tag files
rm -rf tags
mkdir tags

# Generate a list of all files below the current directory (recursively) ending
# in .c, .h. $PWD forces find to generate a list of absolute paths
fd -E "build/*" -e c -e h . $PWD > tags/files.out

# Run ctags recursivly to generate ctags file
# ctags -L "tags/files.out" -f "tags/tags"
ptags -t 16 -f "$PWD/tags/tags"

# Run cscope recursivly (-b turns the GUI off). 
cscope -b -q -k -i "tags/files.out" -f "tags/cscope.out"
