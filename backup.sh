#!/bin/bash
cd ~
cp -r scripts/backup.sh scripts/update_tools.sh .fbsite .fitbit_fw.yaml .snowflake ~/fb_configs
cp -r .zshrc .zprofile .vimrc .profile .tmux.conf .viminfo .gitconfig .gdbinit .gdb_history ~/configs/
cp .i3/config ~/configs/i3config
cp .config/alacritty/alacritty.yml ~/configs/alacritty.yml
