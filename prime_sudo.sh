#!/bin/sh

if [ $1 == "start" ]; then
  sudo -v

  if [ $? -ne 0 ]; then
    echo "Error"
    exit 1
  fi

  /home/shiva/scripts/pump_sudo.sh &
  echo $! > /tmp/prime_sudo_pid
  echo "started sudo primer with pid"
elif [ $1 == "stop" ]; then
  kill `cat /tmp/prime_sudo_pid`
  echo "stopped sudo primer"
fi
